# CP4PP repository #

**CP4PP:** Constraint Programming for Production Planning

This repository contains all source codes for the CP models, global constraints (StockingCost and IDStockingCost constraints) and the instances used for our experiments in the paper: [The Item Dependent StockingCost Constraint](Link URL).

The implementations and tests have been realized within the [OscaR open source solver](https://bitbucket.org/oscarlib/oscar/wiki/Home).

### Usage ###

To test the different models with the replay strategy as described in the [IDStockingCost](Link URL) paper
(assuming that sbt and scala are well installed), 
please follow these instructions:

```
#!sh
    Clone the current repository : hg clone https://bitbucket.org/ratheilesse/cp4pp/
    Move into the clone's directory
    sbt
    update
    compile
    run <File> -t <timeout> -h <constantStockingCost>
```

in which:

```
#!sh

 <File>
        the input instance. It can be "instancesWith20periods/i" or "instancesWith500periods/i" with i in [1,...,100].
 -t <timeout> 
        Optional. The timeout (in seconds) to record the tree by the baseline model. 
        By default timeout = 60.
 -h <constantStockingCost>
        Optional. Only if you want to use the same per period stocking cost. 
        In that case, the model with the "StockingCost" constraint will be run also.
```

To test each model individually, use the "-m" option:


```
#!sh

run <File> -t <timeout> -h <constantStockingCost> -m <model>

```

with

```
#!sh

 -m <model>
        Optional. 1 => IDStockingCost based model. 2 => MinimumAssignment based model. 3. Basic model. 4 => StockingCost based model (with constant per period stocking cost).


```


To work on the source code with [IntelliJ Idea](https://www.jetbrains.com/idea/download/) or [eclipse](http://scala-ide.org/download/sdk.html):

```
#!sh
    Download and install the latest version of IntelliJ Idea or eclipse
    Move into the clone's directory
    sbt 
    update
    compile
    run
    gen-idea // for IntelliJ Idea
    eclipse // for Eclipse
    import project in your IDE and run it
```


### Questions/Comments ###
For any question/comment, feel free to write to [ratheilesse@gmail.com](Link URL) or [pschaus@gmail.com](Link URL)
