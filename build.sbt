lazy val root = (project in file(".")).

settings(
  mainClass in (Compile, packageBin) := Some("MI_DLS_CC_SC_replay_Main"),
assemblyMergeStrategy in assembly := {
case x if x.endsWith(".class") => MergeStrategy.first
case x =>
val oldStrategy = (assemblyMergeStrategy in assembly).value
oldStrategy(x)
},
  name := "productionPlanningByCP",
  scalaVersion := "2.11.4",
  scalacOptions in Compile ++= Seq(
    "-optimise",
    "-Xelide-below", "3000",
    "-Xdisable-assertions"),
  javaOptions in run += "-Xmx8G",
  resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot-local/",
  libraryDependencies += "oscar" %% "oscar-cp" % "xcsp3-SNAPSHOT" withSources(),
  libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.+" % Test,
  libraryDependencies += "dk.brics.automaton" % "automaton" % "1.11-8",
  libraryDependencies += "com.github.scopt" %% "scopt" % "3.3.0"
)
