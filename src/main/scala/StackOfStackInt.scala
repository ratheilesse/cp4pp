/**
  * Data structure to represent a stack of stack of integer values.
  * For instance [[1,4,2],[5,8]] has two stacks.
  * Its size is 5, the top-post stack has a size of 2.
  * After one pop the status is [[1,4,2],[5]] and the top most stack has size 1
  * After another pop the status is [[1,4,2]] and the top most stack has a size of 3
  * @author Ratheil Houndji
  * @author Pierre Schaus
  */
class StackOfStackInt(n: Int) {

  if (n < 1) throw new IllegalArgumentException("n should be > 0")

  private[this] var mainStack: Array[Int] = Array.ofDim(n)
  private[this] var cumulSizeOfStack: Array[Int] = Array.ofDim(n)

  private[this] var indexMainStack = 0
  private[this] var nStack = 0

  def push(i: Int): Unit = {
    if (indexMainStack == mainStack.length) grow()
    mainStack(indexMainStack) = i
    indexMainStack += 1
  }

  /**
    * close the current stack (if not empty) and start a new empty one
    */
  def pushStack(): Unit = {
    if (indexMainStack != 0 && cumulSizeOfStack(nStack) != indexMainStack) {
      nStack += 1
      cumulSizeOfStack(nStack) = indexMainStack
    }
  }

  def isEmpty(): Boolean = {
    indexMainStack == 0
  }

  def size(): Int = {
    indexMainStack
  }

  /**
    * Pop the top element of the top stack
    * @return the value of the top element on the top stack
    */
  def pop(): Int = {
    if (indexMainStack == 0) throw new NoSuchElementException("Stack empty")
    else {
      if (cumulSizeOfStack(nStack) == indexMainStack) nStack -= 1
      indexMainStack -= 1
      mainStack(indexMainStack)
    }
  }

  /**
    * @return The number of stacks that are stacked
    */
  def nStacks(): Int = {
    nStack
  }

  /**
    * @return The size of the top stack
    */
  def sizeTopStack(): Int = {
    if (cumulSizeOfStack(nStack) == indexMainStack) {
      cumulSizeOfStack(nStack) - cumulSizeOfStack(nStack - 1)
    } else {
      indexMainStack - cumulSizeOfStack(nStack)
    }
  }

  def reset(): Unit = {
    indexMainStack = 0
    nStack = 0
  }

  private def grow(): Unit = {
    val newStack = new Array[Int](indexMainStack * 2)
    System.arraycopy(mainStack, 0, newStack, 0, indexMainStack)
    mainStack = newStack

    val newCumulSizeOfStack = new Array[Int](indexMainStack * 2)
    System.arraycopy(cumulSizeOfStack, 0, newCumulSizeOfStack, 0, indexMainStack)
    cumulSizeOfStack = newCumulSizeOfStack
  }

}

/*object testStackOfStackInt extends App {

  val test = new StackOfStackInt(3)

  test.push(3)
  test.push(5)
  test.pushStack()
  test.pushStack()
  println("sizeOfCurrentStack1: " + test.sizeTopStack)
  test.push(44)
  test.push(22)
  test.push(34)
  println("sizeOfCurrentStack2: " + test.sizeTopStack)
  test.push(5)
  test.pushStack()
  test.pushStack()
  test.push(49)
  test.push(57)
  test.pushStack()

  val nStacks = test.nStacks()
  println("in : " + nStacks)
  var i = 1
  while (i <= nStacks) {
    val sizeOfCurrentStack = test.sizeTopStack
    println("sizeOfCurrentStack3: " + sizeOfCurrentStack)
    println(test.pop)
    var j = 1
    while (j < sizeOfCurrentStack) {
      println(test.pop)
      j += 1
    }
    i += 1
  }

  val size = test.size()
  for (i <- 1 to size) {
    println(test.pop)
  }

  println("Done")
}*/