import java.io.File

import oscar.algo.search.DFSLinearizer
import oscar.cp._
import oscar.cp.constraints.MyMinCircuit

import scala.io.Source


/**
  *
  * The Capacitated Lot Sizing and Scheduling Problem (CLSP) is the problem of
  * determining a minimal cost production schedule, such that
  * machine capacity restrictifons are not violated, and
  * demand for all products is satisfied without backlogging.
  * The planning horizon is discrete and finite.
  * The variant considered here
  * - called Pigment Sequencing Problem (PSP), in Production Planing by MIP, Pochet and Wolsey, Springer 2005 -
  * is a multi-item, single machine problem with capacity of production limited to one per period.
  *
  * There are item depedent stocking costs and sequence-dependent changeover costs.
  *
  * @author Vinasetan Ratheil Houndji, ratheilesse@gmail.com1
  * @author Pierre Schaus, pschaus@gmail.com
  *
  */

case class Param(inputFile: File = new File("."),
                  timeout: Int = 60,
                  constantStockingCost: Int = 0,
                 model: Int = 0)

object MI_DLS_CC_SC_replay_Main extends App {

  val parser = new scopt.OptionParser[Param]("CP4PP") {
    head("CP4PP", "1.0")

    arg[File]("<Input File>") action { (x, c) =>
      c.copy(inputFile = x)
    } validate { x =>
      if (x.exists()) success else failure("<Input File> does not exist")
    } text ("the input instance")
    opt[Int]('t', "<the time limit>") action { (x, c) =>
      c.copy(timeout = x)
    } validate { x =>
      if (x > 0) success else failure("the timeout must be > 0")
    }
    opt[Int]('h', "<the constant per period StockingCost>") action { (x, c) =>
      c.copy(constantStockingCost = x)
    } validate { x =>
      if (x > 0) success else failure("the stockingCost must be > 0")
    }
    opt[Int]('m', "<the model to use>") action { (x, c) =>
      c.copy(model = x)
    } validate { x =>
      if (x > 0) success else failure("the model number is in [1,...,3]")
    }

    //override def showUsageOnError = true
  }

  parser.parse(args, Param()) match {
    case Some(config) =>

      implicit val cp = CPSolver()
      cp.silent = true
      //******************** Problem Data *******************************
      val src = config.inputFile
      val lines = Source.fromFile(src).getLines.reduceLeft(_ + " " + _)
      val vals = lines.split("[ ,\t]").toList.filterNot(_ == "").map(_.toInt)
      var index = 0
      def next() = {
        index += 1
        vals(index - 1)
      }
      //  println(vals)
      val nPeriods = next()
      val nItems = next()
      val nDemands = next()

      val changeOverCost = Array.tabulate(nItems, nItems) { case (i, j) => next }
      val h = Array.fill(nItems)(next)

      if(config.constantStockingCost != 0) {
        for (i <- 0 until (nItems)) h(i) = 1
      }

      val demandsByItem = Array.fill(nItems, nPeriods)(next)

      //val nbDemands = demandsByItem.flatten.filter(_ == 1).size
      val stockingCost: Array[Int] = Array.fill(nDemands)(0)
      val deadline: Array[Int] = Array.fill(nDemands)(0)
      val item: Array[Int] = Array.fill(nDemands)(0)
      //val nDemandsByItem = Array.fill(nItems)(0)
      var d = 0
      for (i <- 0 until nItems; t <- 0 until nPeriods; if (demandsByItem(i)(t) == 1)) {
        deadline(d) = t
        item(d) = i
        //  nDemandsByItem(i) += 1
        stockingCost(d) = h(i)
        d += 1
      }

      val costMatrixStock = Array.tabulate(nDemands) { d =>
        Array.tabulate(nPeriods)(t => if (t <= deadline(d)) ((deadline(d) - t) * stockingCost(d)) else 1000000)
      }
      val costMatrixChangeover = Array.tabulate(nDemands + 1, nDemands + 1) {
        case (i, j) =>
          if (i == nDemands || j == nDemands) 0
          else changeOverCost(item(i))(item(j))
      }
      //***********************************

      val date: Array[CPIntVar] = Array.tabulate(nDemands)(d => CPIntVar((0 to nPeriods).filterNot(t => t > deadline(d)))(cp)) :+ CPIntVar(1 to nPeriods)(cp)
      val objStock = CPIntVar(0 until 1000000)(cp)

      var nbSols = 0

      var best = Array.fill(nDemands)(0)
      var bestCost = Integer.MAX_VALUE

      val successors = Array.tabulate(nDemands + 1)(i => CPIntVar(0 to nDemands)(cp))
      val preds = Array.tabulate(nDemands + 1)(i => CPIntVar(0 to nDemands)(cp))

      val objChangeOver = CPIntVar(0 until changeOverCost.flatten.max * nPeriods)(cp)

      //cp.add(minCircuit(successors,costMatrixChangeover, objChangeOver, false), Weak);
      cp.add(new MyMinCircuit(successors, costMatrixChangeover, objChangeOver, false), Weak)

      for (d <- 0 until nDemands) {
        cp.add(date(d) < date(successors(d)))
      }

      for (d <- 0 until (nDemands - 1); if (item(d) == item(d + 1))) {
        cp.add(date(d) < date(d + 1))
      }
      for (n1 <- 0 until (nDemands - 1); n2 <- n1 until nDemands; if (item(n1) == item(n2))) {
        cp.add(successors(n2) !== n1)
      }

      // objective

      cp.add(objChangeOver === sum(0 until nDemands)(i => costMatrixChangeover(i)(successors(i))))
      val obj  = if(config.constantStockingCost != 0) {
        objChangeOver + objStock * config.constantStockingCost
      } else {
        objChangeOver + objStock
      }
      cp.minimize(obj)

      cp.onSolution {
        //println(date.mkString(","))
        best = date.map(_.value)
        bestCost = obj.value
      }

      //Search
      cp.search {
        //binaryStatic(date, _.min)
        //binaryFirstFail(date, _.min) //++ binaryFirstFail(successors, i => successors(i).min)
        conflictOrderingSearch(date, i => date(i).size, i => date(i).min) //++ conflictOrderingSearch(successors, i => successors(i).size, i => successors(i).min)
      }

      val lin = new DFSLinearizer()

      val stats = startSubjectTo(timeLimit = config.timeout, searchListener = lin) {
        cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
        if(config.model == 0 || config.model == 3){
          cp.add(allDifferent(date), Weak)
        } else if(config.model == 1){
          cp.add(allDifferent(date), Medium)
          cp.add(new IDStockingCost(date.filterNot(_ == date(nDemands)), deadline, stockingCost, objStock, Array.fill((nPeriods + 1))(1)))
        } else if(config.model == 2){
          cp.add(allDifferent(date), Medium)
          cp.add(minAssignment(date.filterNot(_ == date(nDemands)), costMatrixStock, objStock), Weak)
        } else if(config.model == 4){
          cp.add(new StockingCost(date.filterNot(_ == date(nDemands)), deadline, objStock, 1))
        }
      }
      println("=================================================")
      println(stats)
      println("bestCost: " + bestCost)
      println(best.mkString(" , ") + " => " + bestCost)

      if(config.model == 0) {
        println("recorded tree: " + src)
        println("--------------")
        // --------------------------------
        cp.obj(obj).relax
        val stat0 = cp.replaySubjectTo(lin, date) {
          cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
          cp.add(allDifferent(date), Weak)
        }
        println("Basic: \n" + stat0)

        /*
      // not as efficient as the decomposition without element
      cp.obj(obj).relax
      val stat0 = cp.replaySubjectTo(lin, date) {
        cp.add(objStock === sum(0 until nDemands)(i => costMatrixStock(i)(date(i))))
        cp.add(allDifferent(date), Weak)
      }
      println(stat0)*/

        if (config.constantStockingCost != 0) {
          cp.obj(obj).relax
          val stat1 = cp.replaySubjectTo(lin, date) {
            //cp.add(objStock === sum(0 until nDemands)(i => costMatrixStock(i)(date(i))))
            cp.add(allDifferent(date), Weak)
            cp.add(new StockingCost(date.filterNot(_ == date(nDemands)), deadline, objStock, 1))
            cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
          }
          println("StockingCost: \n" + stat1)
        }

        cp.obj(obj).relax
        val stat2 = cp.replaySubjectTo(lin, date) {
          cp.add(allDifferent(date), Medium)
          cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
          cp.add(new IDStockingCost(date.filterNot(_ == date(nDemands)), deadline, stockingCost, objStock, Array.fill((nPeriods + 1))(1)))
        }
        println("IDS: \n" + stat2)


        cp.obj(obj).relax
        val stat3 = cp.replaySubjectTo(lin, date) {
          cp.add(allDifferent(date), Medium)
          cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
          cp.add(minAssignment(date.filterNot(_ == date(nDemands)), costMatrixStock, objStock), Weak)
        }
        println("MinAssBC: \n" + stat3)

 /*         cp.obj(obj).relax
          val stat4 = cp.replaySubjectTo(lin, date) {
            cp.add(allDifferent(date), Medium)
            cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
            cp.add(minAssignment(date.filterNot(_ == date(nDemands)), costMatrixStock, objStock), Weak)
            cp.add(new IDStockingCost(date.filterNot(_ == date(nDemands)), deadline, stockingCost, objStock, Array.fill((nPeriods + 1))(1)))
          }
          println("MinAss+IDS: \n" + stat4)


            cp.obj(obj).relax
      val stat5 = cp.replaySubjectTo(lin, date) {
        cp.add(allDifferent(date), Strong)
        cp.add(objStock === -sum(0 until nDemands)(d => (date(d) - deadline(d)) * stockingCost(d)))
        cp.add(minAssignment(date.filterNot(_ == date(nDemands)), costMatrixStock, objStock), Strong)
      }
      println("MinAssStrong: \n" + stat5)
*/
        println("\nfinished")
      }
    case None =>
    //parser.showUsage

  }


}